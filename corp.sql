/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 5.7.22-0ubuntu0.16.04.1 : Database - corp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`corp` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `corp`;

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '1',
  `category_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_alias_unique` (`alias`),
  KEY `articles_user_id_foreign` (`user_id`),
  KEY `articles_category_id_foreign` (`category_id`),
  CONSTRAINT `articles_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `articles` */

insert  into `articles`(`id`,`title`,`text`,`desc`,`alias`,`img`,`created_at`,`updated_at`,`user_id`,`category_id`) values 
(1,'first article','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto, at eum facere inventore nemo nostrum officia perferendis quisquam similique? A error quisquam tempora? Aliquam, aspernatur beatae blanditiis consectetur culpa dicta ea earum, illo ipsa ipsam iste iusto maiores obcaecati odio odit perspiciatis, praesentium quam quidem quisquam quos repellat rerum saepe suscipit ullam voluptate. Asperiores, consequatur excepturi fugiat illo iure nobis odit provident quae quam quia rerum sint sunt veniam. Beatae corporis id minima officiis possimus quaerat quos sequi soluta!</p>','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dolor doloribus error expedita nemo, quisquam suscipit vitae? Atque consectetur minima nulla rerum sequi ut voluptas!</p>','article-1','{\"mini\":\"0037-55x55.jpg\",\"max\":\"0081-816x282.jpg\",\"path\":\"001-700x345.jpg\"}','2018-06-13 08:05:04','2018-06-13 08:05:01',1,1),
(2,'second article','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto, at eum facere inventore nemo nostrum officia perferendis quisquam similique? A error quisquam tempora? Aliquam, aspernatur beatae blanditiis consectetur culpa dicta ea earum, illo ipsa ipsam iste iusto maiores obcaecati odio odit perspiciatis, praesentium quam quidem quisquam quos repellat rerum saepe suscipit ullam voluptate. Asperiores, consequatur excepturi fugiat illo iure nobis odit provident quae quam quia rerum sint sunt veniam. Beatae corporis id minima officiis possimus quaerat quos sequi soluta!</p>','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dolor doloribus error expedita nemo, quisquam suscipit vitae? Atque consectetur minima nulla rerum sequi ut voluptas!</p>','article-2','{\"mini\":\"003-55x55.jpg\",\"max\":\"0061-816x282.jpg\",\"path\":\"0061-700x345.jpg\"}','2018-06-13 08:05:04','2018-06-13 08:05:01',1,1);

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_alias_unique` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`title`,`parent_id`,`alias`,`created_at`,`updated_at`) values 
(1,'Blog',0,'blog',NULL,NULL);

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `article_id` int(10) unsigned NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_article_id_foreign` (`article_id`),
  KEY `comments_user_id_foreign` (`user_id`),
  CONSTRAINT `comments_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `comments` */

/*Table structure for table `filters` */

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filters_alias_unique` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `filters` */

insert  into `filters`(`id`,`title`,`alias`,`created_at`,`updated_at`) values 
(1,'brand identify','brand-identify','2018-06-03 21:50:38','2018-06-03 21:50:41');

/*Table structure for table `menus` */

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menus` */

insert  into `menus`(`id`,`title`,`path`,`parent`,`created_at`,`updated_at`) values 
(1,'Home','',0,'2018-06-04 00:57:47','2018-06-04 00:57:50'),
(2,'Blog','/articles',0,'2018-06-04 00:58:17','2018-06-04 00:58:20'),
(3,'Computers','/articles/cat/computers',2,'2018-06-04 00:59:15','2018-06-04 00:59:18'),
(4,'Portfolio','/portfolios',0,'2018-06-04 00:59:53','2018-06-04 00:59:55'),
(5,'Contacts','/contakts',0,'2018-06-04 01:00:16','2018-06-04 01:00:18');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2018_05_29_121618_create_articles_table',1),
(4,'2018_05_29_122954_create_portfolios_table',1),
(5,'2018_05_29_123642_create_filters_table',1),
(6,'2018_05_29_124131_create_comments_table',1),
(7,'2018_05_29_124506_create_sliders_table',1),
(8,'2018_05_29_124758_create_menus_table',1),
(9,'2018_05_29_124920_create_categories_table',1),
(10,'2018_06_03_153554_change_articles_table',2),
(11,'2018_06_03_154146_change_comments_table',2),
(12,'2018_06_03_154643_change_portfolios_table',3);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `portfolios` */

DROP TABLE IF EXISTS `portfolios`;

CREATE TABLE `portfolios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `filter_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `portfolios_alias_unique` (`alias`),
  KEY `portfolios_filter_alias_foreign` (`filter_alias`),
  CONSTRAINT `portfolios_filter_alias_foreign` FOREIGN KEY (`filter_alias`) REFERENCES `filters` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `portfolios` */

insert  into `portfolios`(`id`,`title`,`text`,`customer`,`alias`,`img`,`created_at`,`updated_at`,`filter_alias`) values 
(3,'Steep This','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto, at eum facere inventore nemo nostrum officia perferendis quisquam similique? A error quisquam tempora? Aliquam, aspernatur beatae blanditiis consectetur culpa dicta ea earum, illo ipsa ipsam iste iusto maiores obcaecati odio odit perspiciatis, praesentium quam quidem quisquam quos repellat rerum saepe suscipit ullam voluptate. Asperiores, consequatur excepturi fugiat illo iure nobis odit provident quae quam quia rerum sint sunt veniam. Beatae corporis id minima officiis possimus quaerat quos sequi soluta!</p>','customer 1','portfolio-1','{\"mini\":\"0061-175x175.jpg\",\"max\":\"0061-770x368.jpg\",\"path\":\"0061.jpg\"}','2018-06-03 21:51:31','2018-06-03 21:51:29','brand-identify'),
(4,'this','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto, at eum facere inventore nemo nostrum officia perferendis quisquam similique? A error quisquam tempora? Aliquam, aspernatur beatae blanditiis consectetur culpa dicta ea earum, illo ipsa ipsam iste iusto maiores obcaecati odio odit perspiciatis, praesentium quam quidem quisquam quos repellat rerum saepe suscipit ullam voluptate. Asperiores, consequatur excepturi fugiat illo iure nobis odit provident quae quam quia rerum sint sunt veniam. Beatae corporis id minima officiis possimus quaerat quos sequi soluta!</p>','customer1','portfolio-2','{\"mini\":\"0011-175x175.jpg\",\"max\":\"0011-175x175.jpg\",\"path\":\"0011.jpg\"}','2018-06-11 22:09:03','2018-06-11 22:09:08','brand-identify'),
(6,'this','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto, at eum facere inventore nemo nostrum officia perferendis quisquam similique? A error quisquam tempora? Aliquam, aspernatur beatae blanditiis consectetur culpa dicta ea earum, illo ipsa ipsam iste iusto maiores obcaecati odio odit perspiciatis, praesentium quam quidem quisquam quos repellat rerum saepe suscipit ullam voluptate. Asperiores, consequatur excepturi fugiat illo iure nobis odit provident quae quam quia rerum sint sunt veniam. Beatae corporis id minima officiis possimus quaerat quos sequi soluta!</p>','customer1','portfolio-3','{\"mini\":\"009-175x175.jpg\",\"max\":\"009-770x368.jpg\",\"path\":\"009.jpg\"}','2018-06-11 22:09:03','2018-06-11 22:09:08','brand-identify'),
(8,'this','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto, at eum facere inventore nemo nostrum officia perferendis quisquam similique? A error quisquam tempora? Aliquam, aspernatur beatae blanditiis consectetur culpa dicta ea earum, illo ipsa ipsam iste iusto maiores obcaecati odio odit perspiciatis, praesentium quam quidem quisquam quos repellat rerum saepe suscipit ullam voluptate. Asperiores, consequatur excepturi fugiat illo iure nobis odit provident quae quam quia rerum sint sunt veniam. Beatae corporis id minima officiis possimus quaerat quos sequi soluta!</p>','customer1','portfolio-4','{\"mini\":\"0081-175x175.jpg\",\"max\":\"0081-770x368.jpg\",\"path\":\"0081.jpg\"}','2018-06-11 22:09:03','2018-06-11 22:09:08','brand-identify'),
(9,'this','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto, at eum facere inventore nemo nostrum officia perferendis quisquam similique? A error quisquam tempora? Aliquam, aspernatur beatae blanditiis consectetur culpa dicta ea earum, illo ipsa ipsam iste iusto maiores obcaecati odio odit perspiciatis, praesentium quam quidem quisquam quos repellat rerum saepe suscipit ullam voluptate. Asperiores, consequatur excepturi fugiat illo iure nobis odit provident quae quam quia rerum sint sunt veniam. Beatae corporis id minima officiis possimus quaerat quos sequi soluta!</p>','customer1','portfolio-5','{\"mini\":\"0071-175x175.jpg\",\"max\":\"0071-770x368.jpg\",\"path\":\"0071.jpg\"}','2018-06-11 22:09:03','2018-06-11 22:09:08','brand-identify');

/*Table structure for table `sliders` */

DROP TABLE IF EXISTS `sliders`;

CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sliders` */

insert  into `sliders`(`id`,`img`,`desc`,`title`,`created_at`,`updated_at`) values 
(1,'xx.jpg','lorem ipsum','Title','2018-06-04 01:01:29','2018-06-04 01:01:31'),
(2,'dd.jpg','lorem ','Title-2','2018-06-10 17:01:52','2018-06-10 17:01:54'),
(3,'00314.jpg','lorem3','Title-3','2018-06-10 17:04:42','2018-06-10 17:04:45');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'user','user@mail.com','$2y$10$vhp0Yye8du/6qmoG.AttXuQgUaMeDJ4gN9cVYqDiohR8jZNVYU66O',NULL,'2018-06-03 15:52:28','2018-06-03 15:52:28');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
